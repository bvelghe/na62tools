# Job Output Checker (JOC)
# bob.velghe@triumf.ca - June 5th, 2017
import sqlite3
import os
import htcondor
import re

#FIXME: read ds_path from config.json

ds_path = '/eos/experiment/na62/user/b/bvelghe/pre-proc/'

def conn_ready(func):
    def func_wrapper(*args, **kwargs):
        if(args[0].conn is None):
            raise RuntimeError(func.func_name+': sqlite db connection not open' )
        return func(*args, **kwargs)
    return func_wrapper

class JobOutputChecker:
    conn = None
    schedd = None

    status_map = {}
    job_id_re = re.compile(r'.*job_(.+).sh$')
    
    def format_filename(self,string):
        if len(string) > 23:
            return string[0:10]+'...'+string[-10:]
        return string

    def get_condor_status(self):
        if(self.schedd is None):
            self.schedd = htcondor.Schedd() 
        jobs = self.schedd.query('Owner=?="bvelghe"')
        return dict([(self.job_id_re.match(job['Cmd']).group(1),job['JobStatus']) for job in jobs])

    def open(self,db_file_name):
        if(self.conn is None):
            self.conn = sqlite3.connect(db_file_name)
            self.conn.row_factory = sqlite3.Row
        else:
            raise RuntimeError('open: sqlite db connection already open')

    @conn_ready
    def close(self):
        self.conn.close()

    def check_eos_output(self,outfile):
        try:
            ondisk_size = os.path.getsize(ds_path+outfile)
        except OSError:
            return False
        return ondisk_size

    #FIXME: Decouple the display and the logic
    @conn_ready
    def list_jobs(self,batch):
        cur = self.conn.cursor()
        outbuf = ''
        summary = 'Ok'
        for row in cur.execute('SELECT job_id,date,analyzer,output,config_string,note FROM joblog WHERE batch_id = ?',(batch,)):
            try:
                condor_status = self.status_map[row['job_id']]
            except KeyError:
                condor_status = -1

            global_state = 'Unknown'
            # Do we need to check the disk?
            if(condor_status == 4 or condor_status == -1):
                if(self.check_eos_output(row['output']) > 0):
                    global_state = 'Done'
                else:
                    global_state = 'Failed'
                    summary = 'Failed'
            
            elif(condor_status == 1):
                global_state = 'Idle'
            elif(condor_status == 2):
                global_state = 'Running'

            outbuf += '| '+row['job_id']+' | '+row['analyzer']+' | '+self.format_filename(row['output'])+' | '+row['config_string']+' | ' + global_state + ' |\n'
                     
        return (summary,outbuf)

    @conn_ready
    def list_batchs(self):
        cur = self.conn.cursor()
        self.status_map = self.get_condor_status()
        outbuf = ''
        for row in cur.execute('SELECT DISTINCT batch_id FROM joblog WHERE status < 0'):
            (job_state,job_printout) = self.list_jobs(row['batch_id'])
            outbuf += '---- Batch '+str(row['batch_id'])+' ['+job_state+'] ----\n'
            outbuf += job_printout
        print outbuf

    #FIXME [DEPRECIATED] This is duplicated in make_list.py --> Do something!
    def check_config_file(value):
        try:
            with open(value) as config_file:
                config = json.load(config_file)
        except EnvironmentError:
            raise argparse.ArgumentTypeError("unable to read the configuration file %s" % value)
        config_keys = ['na62_analysis_path', # full path of the analysis directory
                   'output_files_path', # full path of the output directory
                   'temp_file_dir', # full path of the temporary files directory
                   'cmd_line', # analyzer generic run command,
                   'output_file_suffix', # output file suffix (e.g. '.root')
                   'log_db', # name of the jobs db file
                   ]
        for key in config_keys:
            if not key in config:
                raise argparse.ArgumentTypeError("key %s is not present in %s" % (key,value))
        return config


