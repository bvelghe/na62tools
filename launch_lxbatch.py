# Run analysis jobs on lxbatch
# bob.velghe@triumf.ca - May 30th, 2016

from __future__ import with_statement
import sys
import argparse
import json
import tempfile
import os
import math
from string import Template
from datetime import datetime
import uuid
import subprocess
import re
import joblogger
import time

###########
# Globals #
###########
tmp_dir = None

###########
# Helpers #
###########
def load_file_list(file_name):
    with open(file_name) as input_file:
        input_files = input_file.readlines()
    return input_files
 
def prepare_inputs(input_list,batch_size):
    global tmp_dir
    prep_inputs = []
    if(not tmp_dir):
        raise ValueError('the environment is not setup')
    N = int(math.ceil(len(input_list)/float(batch_size)))
    for part in range(N):
        cur_file = tempfile.NamedTemporaryFile(mode='w+r',prefix='input_',dir=tmp_dir,delete=False)
        prep_inputs.append(cur_file.name)
        for i in range(batch_size):
            try:
                cur_file.write(input_list.pop())
            except IndexError:
                break
    return prep_inputs


#################
# Inputs checks #
#################
def check_config_file(value):
    try:
        with open(value) as config_file:
            config = json.load(config_file)
    except EnvironmentError:
        raise argparse.ArgumentTypeError("unable to read the configuration file %s" % value)
    
    config_keys = ['na62_analysis_path', # full path of the analysis directory
                   'output_files_path', # full path of the output directory
                   'temp_file_dir', # full path of the temporary files directory
                   'cmd_line', # analyzer generic run command,
                   'ht_condor_sub', # HT Condor .sub file template
                   'output_file_suffix', # output file suffix (e.g. '.root')
                   'log_db', # name of the jobs db file
                   ]
    for key in config_keys:
        if not key in config:
            raise argparse.ArgumentTypeError("key %s is not present in %s" % (key,value))
    return config

def check_batch_size(value):
    try:
        ivalue = int(value)
    except:
        raise argparse.ArgumentTypeError("%s is an invalid positive integer value" % value)
    if ivalue < 1:
        raise argparse.ArgumentTypeError("%s is an invalid positive integer value" % value)
    return ivalue

def check_analyzer_name(value):
    #TODO Add checks
    return value


def check_input_list(value):
    try:
        input_files = load_file_list(value)
    except EnvironmentError:
        raise argparse.ArgumentTypeError("unable to read the input files list %s" % value)
    return input_files
        
########
# Main #
########

def main():
    global tmp_dir 
    parser = argparse.ArgumentParser(description='Launch NA62 analysis jobs on HTCondor (or lxbatch, see -o option).')
    parser.add_argument('-t','--test', dest='is_test', action='store_true', help='send a single job to the test queue')
    parser.add_argument('-s','--size', dest='batch_size', type=check_batch_size, default=1, help='number of bursts per job')
    parser.add_argument('-c','--config', dest='config_file', type=check_config_file, default='config.json', help='configuration file')
    parser.add_argument('-n','--note', dest='note', default='', help='attach a note to this batch')
    parser.add_argument('-q','--queue', dest='lxbatch_queue', default='1nh', help='specify the lxbatch queue to use')
    parser.add_argument('-W','--runtime', dest='lxbatch_runtime_limit', default='2:00', help='specify the lxbatch runtime limit')
    parser.add_argument('-d','--dry', dest='is_dry', action='store_true', help='dry run')
    parser.add_argument('-p','--config-string', dest='config_string', default='', help='Analyzer configuration string')
    parser.add_argument('-l','--no-filtering', dest='no_filtering', action='store_true', help='Disable the filtering')
    parser.add_argument('-o','--legacy', dest='legacy', action='store_true', help='Run on lxbatch')
    parser.add_argument('analyzer_name', type=check_analyzer_name, help='name of the analyzer')
    parser.add_argument('input_list', type=check_input_list, help='path of the file list to analyze')
    args = parser.parse_args()

    # Setup the working directory
    tmp_dir = tempfile.mkdtemp(prefix='batch_',dir=args.config_file['temp_file_dir'])

    # Initialise the logger
    logger = joblogger.JobLogger()
    logger.open(args.config_file['log_db'])
    logger.init()
    
    # Prepare the input lists
    prep_inputs = prepare_inputs(args.input_list,args.batch_size)
    
    # Submit only one job in test mode
    if(args.is_test):
        prep_inputs = [prep_inputs[0]]
        queue = 'test'
        job_duration = args.lxbatch_runtime_limit
    else:
        queue = args.lxbatch_queue
        job_duration = args.lxbatch_runtime_limit

    # Prepare the run commands
    
    config_string = ''
    if(not args.no_filtering):
        config_string += ' --filter'
    if(args.config_string != ''):
        config_string += ' -p "'+args.config_string+'"'
    
    cmd = Template(args.config_file['cmd_line'])
    ht_condor_cmd = Template(args.config_file['ht_condor_sub'])
    
    # Store the name of the output files
    out_file_lst = tmp_dir+'.lst'
    try:
        out_file_list = open(out_file_lst,'w')
    except IOError:
        print 'Unable to create the output file list'
        return -1
        
   
    batch_id_re = re.compile(r'.*batch_(.+)$')
    job_id_re = re.compile(r'.*job_(.+).sh$')
    sub_id_re = None # Future
    for jid,part_input_list in enumerate(prep_inputs):
        cur_file = tempfile.NamedTemporaryFile(prefix='job_',suffix='.sh',dir=tmp_dir,delete=False)
        os.chmod(cur_file.name,0755)
        # Construct the output file name
        analyzer = args.analyzer_name.lower()
        current_time = datetime.now()
        time_tag = datetime.strftime(current_time,'%Y-%m-%d_%H-%M')
        uid = str(uuid.uuid4())
        ujid = str(jid)
        out_file_name = '_'.join([analyzer,time_tag,ujid,uid])+args.config_file['output_file_suffix']
        output_file = out_file_name
            
        # Write the launch shell script on disk
        cur_file.write(cmd.substitute(ANALYSIS_PATH=args.config_file['na62_analysis_path'],ANALYZER=args.analyzer_name,INPUT_LIST=part_input_list,OUTPUT_FILE=output_file,CONFIG_STRING=config_string))
        cur_file.flush()
        job_id = job_id_re.match(cur_file.name).group(1)
        errcode = -1
        batch_id = batch_id_re.match(tmp_dir).group(1)
     
        if(not args.is_dry):
            # Log the operation
            logger.log(job_id,batch_id,-1,current_time.isoformat(),args.analyzer_name,str([fi.strip() for fi in load_file_list(part_input_list)]),output_file,config_string,args.note,-1)
        
            # Add the output file on the list
            out_file_list.write(output_file+'\n')       
 

        if(args.legacy):
            if(not args.is_dry):
                proc = subprocess.Popen(['bsub','-q '+queue,'-W '+job_duration,cur_file.name], cwd=tmp_dir,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                proc.wait()
                out,err=proc.communicate()
                errcode = proc.returncode 
                # Get the job id back from bsub
                m = re.search(r'<([0-9]+)>',out)
                sub_id = m.group(1)
                if(errcode >= 0):
                    print 'Batch '+batch_id+' submitted to lxbatch queue '+queue+' (job ID '+sub_id+')'
                    logger.set_sub_id(batch_id,sub_id)
                    # Wait one second before submitting the next job
                    time.sleep(1)

            else:
                print 'bsub -q '+queue+' -W '+job_duration+' '+cur_file.name


    if(not args.legacy):
        # Create log/output/error directories
        os.mkdir(tmp_dir+'/output')
        os.mkdir(tmp_dir+'/error')
        os.mkdir(tmp_dir+'/log')
        with open(tmp_dir+'/'+batch_id+'.sub','w') as ht_condor_sub_file:
            ht_condor_sub_file.write(ht_condor_cmd.substitute(BATCH_DIR=tmp_dir))
            ht_condor_sub_file.flush()
            if(not args.is_dry):
                proc = subprocess.Popen(['condor_submit',tmp_dir+'/'+batch_id+'.sub'],cwd=tmp_dir,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                proc.wait()
                out,err=proc.communicate()
                errcode = proc.returncode
                # Get the job id back from condor_submit
                # FIXME: This is not robust, use regex as above !
                sub_id = out.split(' ')[-1][:-2]
                if(errcode >= 0):
                    print 'Batch '+batch_id+' submitted to HTCondor (job ID '+sub_id+')'
                    logger.set_sub_id(batch_id,sub_id)
            else:
                print 'condor_submit '+tmp_dir+'/'+batch_id+'.sub'

   
    logger.close()
    return 0

if __name__ == '__main__':
    sys.exit(main())
