# Basic logging infrastructure
# bob.velghe@triumf.ca - May 31th, 2016
import sqlite3

def conn_ready(func):
    def func_wrapper(*args, **kwargs):
        if(args[0].conn is None):
            raise RuntimeError(func.func_name+': sqlite db connection not open' )
        return func(*args, **kwargs)
    return func_wrapper

class JobLogger:
    conn = None

    def open(self,db_file_name):
        if(self.conn is None):
            self.conn = sqlite3.connect(db_file_name)
        else:
            raise RuntimeError('open: sqlite db connection already open')

    @conn_ready
    def close(self):
        self.conn.close()

    @conn_ready
    def init(self):
        cur = self.conn.cursor()
        cur.execute('CREATE TABLE IF NOT EXISTS joblog (job_id integer, batch_id text, sub_id integer, date text, analyzer text, inputs text, output text, config_string text, note text, status integer)')
        self.conn.commit()

    @conn_ready
    def set_sub_id(self,batch_id,sub_id):
        cur = self.conn.cursor()
        cur.execute('UPDATE joblog SET sub_id = ? WHERE batch_id = ?',(sub_id,batch_id))
        self.conn.commit()

    @conn_ready
    def log(self,job_id,batch_id,sub_id,date,analyzer,inputs,output,config_string,note,status=-1):
        cur = self.conn.cursor()
        cur.execute('INSERT INTO joblog VALUES (?,?,?,?,?,?,?,?,?,?)',(job_id,batch_id,sub_id,date,analyzer,inputs,output,config_string,note,status))
        self.conn.commit()
